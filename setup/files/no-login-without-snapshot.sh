#!/bin/bash
set -u
# set -x

motd=/etc/motd
banner=/etc/banner
nologin=/etc/nologin

# if anything fails make sure we don't restrict logins by accident, remove /etc/nologin now
[ -e $nologin ] && rm -f $nologin

me=$(basename $0)
cat >$motd <<EOF

#  VM status unknown, may be master or snapshot
#  source: $0

EOF

cp $motd $banner

smartctl='/usr/sbin/smartctl'
[ -x $smartctl ] || exit 1

disk=/dev/sda
[ -e $disk ] || exit 2

outfile=$(mktemp /tmp/$me.XXXXX)
trap "{ rm -f $outfile; exit $?; }" EXIT SIGINT SIGTERM

$smartctl -i $disk > $outfile
ret=$?
[ $ret -ne 0 ] && exit 3

if grep -q "LU is fully provisioned" $outfile; then
  logger "$0: found fully provisioned system disk $disk. touch $nologin"
  echo "$0: found fully provisioned system disk $disk. touch $nologin"
  touch $nologin
  echo "only root can login as we don't run on a vm snapshot" >$nologin
  echo "try to stop the VM, create a snapshot and start again to fix" >>$nologin

cat >$motd <<EOF

# VM MASTER - take care, no changes
#
# running on master system disk, not on VM snapshot.
# restrict login to root only
#
# no direct changes, always do a snapshot first and later commit
#
# source: $0

EOF
cp $motd $banner

elif grep -q "LU is thin provisioned" $outfile; then

  logger "$0: found thin provisioned system disk $disk. Don't deny logins."
  echo   "$0: found thin provisioned system disk $disk. Don't deny logins."
  cat  >$motd <<EOF

# VM SNAPSHOT
#
# running on thin provisioned snapshot system disk, 
# we  likely run based on a  VM snapshot
# you need to commit changes to make them permanent
# source: $0
EOF

fi

exit 
