# bootstrap a linux VM to setup a  test  ces server - just the initial setup

## what you get
just do what's needed to allow to run ansible playbooks from nas99. And give direct root access to admins. And prevent us to modify the VM master image, modify snapshots only.

- install smartmontools - required to detect if we run on a VM snapshot
- deny non-root logins if we don't run on a VM snapshot: Don't modify the master. Service no-login-without-snapshot.

- install, enable, start vmtoolsd

- create ansible local account
- add ssh public keys to ansible's authorized_keys file: ansible, achrist, hbi4ea
- give ansible via group wheel full password-less sudo access

- add public keys for root's authorized_keys file: hbi4ea, ansible, achrist
- deny ssh password login for root
- set root password

- set LANG and LC_TYPE

## download ansible playbook

login as root
```terminal
# cd /home
#  git clone  https://gitlab.ethz.ch/scaleadmin-ansible-for-ces/vm-initial-setup.git
```
 or
```terminal
# cd /home
# git clone git@gitlab.ethz.ch:scaleadmin-ansible-for-ces/vm-initial-setup.git


```

## get ansible.posix

```terminal
#  https_proxy='https://proxy.ethz.ch:3128' ansible-galaxy collection install ansible.posix
Process install dependency map
Starting collection install process
Installing 'ansible.posix:1.2.0' to '/root/.ansible/collections/ansible_collections/ansible/posix'
```

## run playbook

```terminal
# cd /home/vm-initial-setup
# ./run-setup.sh
```

or just

```terminal
# cd /home/vm-initial-setup
# ansible-playbook -c localhost ./setup.yml
```

please ignore
```
[WARNING]: provided hosts list is empty, only localhost is available. Note that the implicit localhost does not match 'all'
```

example output
```

```

